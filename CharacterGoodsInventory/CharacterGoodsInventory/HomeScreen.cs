﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Windows.Forms;

namespace CharacterGoodsInventory
{
    public partial class HomeScreen : Form
    {
        public HomeScreen()
        {
            InitializeComponent();
        }

        private void ViewPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ViewButton_Click(object sender, EventArgs e)
        {
            SqlCommand getInventory = new SqlCommand("pr_Inventory", DbConnection.Connection)
            {
                CommandType = CommandType.StoredProcedure
            };
            PopulateTable(getInventory);
            ViewSwitch.SelectTab("ViewPage");
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            searchTypeInput.Items.Clear();
            searchSeriesInput.Items.Clear();
            searchComparisonInput.Items.Clear();
            
            DbConnection.OpenConnection();
            List<string> typesList = HelperMethods.GetListItems("pr_GetTypes");
            List<string> seriesList = HelperMethods.GetListItems("pr_GetSeries");
            DbConnection.CloseConnection();

            searchTypeInput.Items.AddRange(typesList.ToArray());
            searchSeriesInput.Items.AddRange(seriesList.ToArray());
            searchComparisonInput.Items.AddRange(new string[] { "More than", "Less than", "Equal to" });

            ViewSwitch.SelectTab("SearchPage");
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            ViewSwitch.SelectTab("AddPage");
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            int goodsId = (int)IdInput.Value;
            object[] goodsParameters =
            {
                descInput.Text,
                typeInput.Text,
                charaInput.Text,
                seriesInput.Text,
                priceInput.Value
            };

            HelperMethods.AddOrUpdateRecord(goodsId, goodsParameters);
            ClearInputs();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            ClearInputs();
            ViewSwitch.SelectTab("HomePage");
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = allData.SelectedRows[0];
            int recordID = (int)row.Cells["ID"].Value;

            HelperMethods.DeleteRecord(recordID);
            allData.Rows.Remove(row);
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = allData.SelectedRows[0];

            IdInput.Value = (int)row.Cells["ID"].Value;
            descInput.Text = (string)row.Cells["Description"].Value;
            typeInput.Text = (string)row.Cells["Type"].Value;
            charaInput.Text = (string)row.Cells["Character"].Value;
            seriesInput.Text = (string)row.Cells["Series"].Value;
            priceInput.Value = (decimal)row.Cells["Price"].Value;

            ViewSwitch.SelectTab("AddPage");
        }

        private void SearchFilterButton_Click(object sender, EventArgs e)
        {
            string[] searchQueries =
            {
                searchKeywordInput.Text,
                searchTypeInput.Text,
                searchCharacterInput.Text,
                searchSeriesInput.Text,
                searchPriceInput.Value.ToString(),
                searchComparisonInput.Text
            };
            
            PopulateTable(HelperMethods.FilterData(searchQueries));
            ClearInputs();
            ViewSwitch.SelectTab("ViewPage");
        }

        //Internal Helper Methods

        private void PopulateTable(SqlCommand command)
        {
            DataTable data = new DataTable();

            DbConnection.OpenConnection();
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(data);
            allData.DataSource = data;
            DbConnection.CloseConnection();

            allData.Columns["ID"].Visible = false;
            allData.Columns["Price"].DefaultCellStyle.Format = "c0";
            allData.Columns["Price"].DefaultCellStyle.FormatProvider = CultureInfo.GetCultureInfo("ja-JP");
        }

        private void ClearInputs()
        {
            //Update or Insert values
            IdInput.Value = 0;
            descInput.Text = string.Empty;
            typeInput.Text = string.Empty;
            charaInput.Text = string.Empty;
            seriesInput.Text = string.Empty;
            priceInput.Value = 0;

            //Search values
            searchKeywordInput.Text = string.Empty;
            searchCharacterInput.Text = string.Empty;
            searchPriceInput.Value = 0;
        }
    }
}
