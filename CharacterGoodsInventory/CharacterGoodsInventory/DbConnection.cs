﻿using System.Data.SqlClient;

namespace CharacterGoodsInventory
{
    public static class DbConnection
    {
        public static string ConnectionString { get; } = System.Configuration.ConfigurationManager.ConnectionStrings["MCSConnection"].ConnectionString;
        public static SqlConnection Connection { get; } = new SqlConnection(ConnectionString);

        public static int ConnectionTimeout = 60;

        public static void OpenConnection()
        {
            Connection.Open();
        }

        public static void CloseConnection()
        {
            Connection.Close();
        }
    }
}
