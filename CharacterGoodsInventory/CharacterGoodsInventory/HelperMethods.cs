﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CharacterGoodsInventory
{
    public static class HelperMethods
    {

        public static void DeleteRecord(int id)
        {
            SqlCommand deleteRow = new SqlCommand("pr_DeleteRow", DbConnection.Connection)
            {
                CommandType = CommandType.StoredProcedure
            };

            deleteRow.Parameters.Add("@VarID", SqlDbType.Int).Value = id;

            DbConnection.OpenConnection();
            deleteRow.ExecuteNonQuery();
            DbConnection.CloseConnection();
        }

        public static void AddOrUpdateRecord(int id, object[] parameters)
        {
            SqlCommand addOrUpdate;

            if (id == 0)
            {
                addOrUpdate = new SqlCommand("pr_Insert", DbConnection.Connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
            }
            else
            {
                addOrUpdate = new SqlCommand("pr_Update", DbConnection.Connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                addOrUpdate.Parameters.Add("@varId", SqlDbType.Int).Value = id;
            }

            addOrUpdate.Parameters.Add("@varDesc", SqlDbType.Text).Value = parameters[0];
            addOrUpdate.Parameters.Add("@varType", SqlDbType.VarChar).Value = parameters[1];
            addOrUpdate.Parameters.Add("@varChara", SqlDbType.VarChar).Value = parameters[2];
            addOrUpdate.Parameters.Add("@varSeries", SqlDbType.VarChar).Value = parameters[3];
            addOrUpdate.Parameters.Add("@varPrice", SqlDbType.Money).Value = parameters[4];

            DbConnection.OpenConnection();
            addOrUpdate.ExecuteNonQuery();
            DbConnection.CloseConnection();
        }

        public static List<string> GetListItems(string procedure)
        {
            List<string> itemList = new List<string>();
            SqlCommand command = new SqlCommand(procedure, DbConnection.Connection)
            {
                CommandType = CommandType.StoredProcedure
            };

            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    itemList.Add(reader.GetString(0));
                }
            }
            else
            {
                itemList.Add("(None found)");
            }
            reader.Close();

            return itemList;
        }

        public static SqlCommand FilterData(string[] parameters)
        {
            for (int i = 0; i < parameters.Length; i++)
            {
                if (string.IsNullOrWhiteSpace(parameters[i]))
                {
                    parameters[i] = null;
                }

                if (i == 0 && parameters[i] != null || i == 2 && parameters[i] != null)
                {
                    parameters[i] = '%' + parameters[i] + '%';
                }
            }

            SqlCommand command = new SqlCommand("pr_Filter", DbConnection.Connection)
            {
                CommandType = CommandType.StoredProcedure
            };

            command.Parameters.Add("@varDesc", SqlDbType.NText).Value = (object)parameters[0] ?? DBNull.Value;
            command.Parameters.Add("@varType", SqlDbType.NVarChar).Value = (object)parameters[1] ?? DBNull.Value;
            command.Parameters.Add("@varChara", SqlDbType.NVarChar).Value = (object)parameters[2] ?? DBNull.Value;
            command.Parameters.Add("@varSeries", SqlDbType.NVarChar).Value = (object)parameters[3] ?? DBNull.Value;
            command.Parameters.Add("@varPrice", SqlDbType.Money).Value = int.Parse(parameters[4]);
            command.Parameters.Add("@varComp", SqlDbType.NVarChar).Value = (object)parameters[5] ?? DBNull.Value;

            return command;
        }
    }
}
