﻿using System.Globalization;

namespace CharacterGoodsInventory
{
    partial class HomeScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.homeInstructions = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.viewButton = new System.Windows.Forms.Button();
            this.searchButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.ViewSwitch = new CharacterGoodsInventory.TablessTabControl();
            this.HomePage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.viewText = new System.Windows.Forms.Label();
            this.searchText = new System.Windows.Forms.Label();
            this.addText = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.viewLabel = new System.Windows.Forms.Label();
            this.searchLabel = new System.Windows.Forms.Label();
            this.addLabel = new System.Windows.Forms.Label();
            this.ViewPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.allData = new System.Windows.Forms.DataGridView();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.editButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.SearchPage = new System.Windows.Forms.TabPage();
            this.searchLayout = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.searchKeywordLabel = new System.Windows.Forms.Label();
            this.searchTypeLabel = new System.Windows.Forms.Label();
            this.searchCharacterLabel = new System.Windows.Forms.Label();
            this.searchSeriesLabel = new System.Windows.Forms.Label();
            this.searchPriceLabel = new System.Windows.Forms.Label();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.searchKeywordInput = new System.Windows.Forms.TextBox();
            this.searchTypeInput = new System.Windows.Forms.ComboBox();
            this.searchCharacterInput = new System.Windows.Forms.TextBox();
            this.searchSeriesInput = new System.Windows.Forms.ComboBox();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.searchComparisonInput = new System.Windows.Forms.ComboBox();
            this.searchPriceInput = new System.Windows.Forms.NumericUpDown();
            this.searchFilterButton = new System.Windows.Forms.Button();
            this.AddPage = new System.Windows.Forms.TabPage();
            this.inputForm = new System.Windows.Forms.TableLayoutPanel();
            this.inputLabels = new System.Windows.Forms.FlowLayoutPanel();
            this.descLabel = new System.Windows.Forms.Label();
            this.typeLabel = new System.Windows.Forms.Label();
            this.characterLabel = new System.Windows.Forms.Label();
            this.seriesLabel = new System.Windows.Forms.Label();
            this.priceLabel = new System.Windows.Forms.Label();
            this.itemInputs = new System.Windows.Forms.FlowLayoutPanel();
            this.descInput = new System.Windows.Forms.TextBox();
            this.typeInput = new System.Windows.Forms.TextBox();
            this.charaInput = new System.Windows.Forms.TextBox();
            this.seriesInput = new System.Windows.Forms.TextBox();
            this.priceInput = new System.Windows.Forms.NumericUpDown();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.saveButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.IdInput = new System.Windows.Forms.NumericUpDown();
            this.homeInstructions.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.ViewSwitch.SuspendLayout();
            this.HomePage.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.ViewPage.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.allData)).BeginInit();
            this.flowLayoutPanel5.SuspendLayout();
            this.SearchPage.SuspendLayout();
            this.searchLayout.SuspendLayout();
            this.flowLayoutPanel6.SuspendLayout();
            this.flowLayoutPanel7.SuspendLayout();
            this.flowLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchPriceInput)).BeginInit();
            this.AddPage.SuspendLayout();
            this.inputForm.SuspendLayout();
            this.inputLabels.SuspendLayout();
            this.itemInputs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.priceInput)).BeginInit();
            this.flowLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IdInput)).BeginInit();
            this.SuspendLayout();
            // 
            // homeInstructions
            // 
            this.homeInstructions.ColumnCount = 2;
            this.homeInstructions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.homeInstructions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.homeInstructions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.homeInstructions.Controls.Add(this.flowLayoutPanel1, 1, 0);
            this.homeInstructions.Controls.Add(this.CloseButton, 1, 1);
            this.homeInstructions.Controls.Add(this.ViewSwitch, 0, 0);
            this.homeInstructions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.homeInstructions.Location = new System.Drawing.Point(0, 0);
            this.homeInstructions.Name = "homeInstructions";
            this.homeInstructions.RowCount = 2;
            this.homeInstructions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.homeInstructions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.homeInstructions.Size = new System.Drawing.Size(734, 411);
            this.homeInstructions.TabIndex = 0;
            this.homeInstructions.Paint += new System.Windows.Forms.PaintEventHandler(this.ViewPanel_Paint);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.viewButton);
            this.flowLayoutPanel1.Controls.Add(this.searchButton);
            this.flowLayoutPanel1.Controls.Add(this.addButton);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(590, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(141, 375);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // viewButton
            // 
            this.viewButton.Location = new System.Drawing.Point(3, 3);
            this.viewButton.Name = "viewButton";
            this.viewButton.Size = new System.Drawing.Size(138, 28);
            this.viewButton.TabIndex = 0;
            this.viewButton.Text = "View All";
            this.viewButton.UseVisualStyleBackColor = true;
            this.viewButton.Click += new System.EventHandler(this.ViewButton_Click);
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(3, 37);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(138, 28);
            this.searchButton.TabIndex = 1;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(3, 71);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(138, 28);
            this.addButton.TabIndex = 2;
            this.addButton.Text = "Add New";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CloseButton.Location = new System.Drawing.Point(590, 384);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(141, 24);
            this.CloseButton.TabIndex = 3;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // ViewSwitch
            // 
            this.ViewSwitch.Controls.Add(this.HomePage);
            this.ViewSwitch.Controls.Add(this.ViewPage);
            this.ViewSwitch.Controls.Add(this.SearchPage);
            this.ViewSwitch.Controls.Add(this.AddPage);
            this.ViewSwitch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ViewSwitch.Location = new System.Drawing.Point(3, 3);
            this.ViewSwitch.Name = "ViewSwitch";
            this.homeInstructions.SetRowSpan(this.ViewSwitch, 2);
            this.ViewSwitch.SelectedIndex = 0;
            this.ViewSwitch.Size = new System.Drawing.Size(581, 405);
            this.ViewSwitch.TabIndex = 4;
            // 
            // HomePage
            // 
            this.HomePage.BackColor = System.Drawing.SystemColors.Menu;
            this.HomePage.Controls.Add(this.tableLayoutPanel1);
            this.HomePage.Location = new System.Drawing.Point(4, 22);
            this.HomePage.Name = "HomePage";
            this.HomePage.Padding = new System.Windows.Forms.Padding(3);
            this.HomePage.Size = new System.Drawing.Size(573, 379);
            this.HomePage.TabIndex = 0;
            this.HomePage.Text = "Home";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(567, 373);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.flowLayoutPanel3.Controls.Add(this.viewText);
            this.flowLayoutPanel3.Controls.Add(this.searchText);
            this.flowLayoutPanel3.Controls.Add(this.addText);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(201, 129);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(354, 114);
            this.flowLayoutPanel3.TabIndex = 7;
            // 
            // viewText
            // 
            this.viewText.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.viewText.AutoSize = true;
            this.viewText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewText.Location = new System.Drawing.Point(3, 0);
            this.viewText.Name = "viewText";
            this.viewText.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.viewText.Size = new System.Drawing.Size(213, 40);
            this.viewText.TabIndex = 7;
            this.viewText.Text = "Returns all database records";
            // 
            // searchText
            // 
            this.searchText.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.searchText.AutoSize = true;
            this.searchText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchText.Location = new System.Drawing.Point(3, 40);
            this.searchText.Name = "searchText";
            this.searchText.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.searchText.Size = new System.Drawing.Size(247, 40);
            this.searchText.TabIndex = 9;
            this.searchText.Text = "Filter results or search by keyword";
            // 
            // addText
            // 
            this.addText.AutoSize = true;
            this.addText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addText.Location = new System.Drawing.Point(3, 80);
            this.addText.Name = "addText";
            this.addText.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.addText.Size = new System.Drawing.Size(257, 40);
            this.addText.TabIndex = 11;
            this.addText.Text = "Add a new records to the database";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.flowLayoutPanel2.Controls.Add(this.viewLabel);
            this.flowLayoutPanel2.Controls.Add(this.searchLabel);
            this.flowLayoutPanel2.Controls.Add(this.addLabel);
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(60, 128);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(135, 116);
            this.flowLayoutPanel2.TabIndex = 6;
            // 
            // viewLabel
            // 
            this.viewLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.viewLabel.AutoSize = true;
            this.viewLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewLabel.Location = new System.Drawing.Point(60, 0);
            this.viewLabel.Name = "viewLabel";
            this.viewLabel.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.viewLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.viewLabel.Size = new System.Drawing.Size(72, 40);
            this.viewLabel.TabIndex = 11;
            this.viewLabel.Text = "View All";
            this.viewLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // searchLabel
            // 
            this.searchLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.searchLabel.AutoSize = true;
            this.searchLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchLabel.Location = new System.Drawing.Point(66, 40);
            this.searchLabel.Name = "searchLabel";
            this.searchLabel.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.searchLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.searchLabel.Size = new System.Drawing.Size(66, 40);
            this.searchLabel.TabIndex = 12;
            this.searchLabel.Text = "Search";
            this.searchLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // addLabel
            // 
            this.addLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.addLabel.AutoSize = true;
            this.addLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addLabel.Location = new System.Drawing.Point(52, 80);
            this.addLabel.Name = "addLabel";
            this.addLabel.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.addLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.addLabel.Size = new System.Drawing.Size(80, 40);
            this.addLabel.TabIndex = 13;
            this.addLabel.Text = "Add New";
            this.addLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ViewPage
            // 
            this.ViewPage.BackColor = System.Drawing.SystemColors.Control;
            this.ViewPage.Controls.Add(this.tableLayoutPanel2);
            this.ViewPage.Location = new System.Drawing.Point(4, 22);
            this.ViewPage.Name = "ViewPage";
            this.ViewPage.Padding = new System.Windows.Forms.Padding(3);
            this.ViewPage.Size = new System.Drawing.Size(573, 379);
            this.ViewPage.TabIndex = 1;
            this.ViewPage.Text = "View";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.allData, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel5, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(567, 373);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // allData
            // 
            this.allData.AllowUserToAddRows = false;
            this.allData.AllowUserToDeleteRows = false;
            this.allData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.allData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.allData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.allData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.allData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.allData.Location = new System.Drawing.Point(3, 3);
            this.allData.MultiSelect = false;
            this.allData.Name = "allData";
            this.allData.ReadOnly = true;
            this.allData.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.allData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.allData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.allData.Size = new System.Drawing.Size(561, 334);
            this.allData.TabIndex = 1;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Controls.Add(this.editButton);
            this.flowLayoutPanel5.Controls.Add(this.deleteButton);
            this.flowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(3, 343);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(561, 27);
            this.flowLayoutPanel5.TabIndex = 2;
            // 
            // editButton
            // 
            this.editButton.Location = new System.Drawing.Point(3, 3);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(141, 24);
            this.editButton.TabIndex = 0;
            this.editButton.Text = "Edit";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.EditButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(150, 3);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(141, 24);
            this.deleteButton.TabIndex = 1;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // SearchPage
            // 
            this.SearchPage.BackColor = System.Drawing.SystemColors.Control;
            this.SearchPage.Controls.Add(this.searchLayout);
            this.SearchPage.Location = new System.Drawing.Point(4, 22);
            this.SearchPage.Name = "SearchPage";
            this.SearchPage.Size = new System.Drawing.Size(573, 379);
            this.SearchPage.TabIndex = 2;
            this.SearchPage.Text = "Search";
            // 
            // searchLayout
            // 
            this.searchLayout.ColumnCount = 2;
            this.searchLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.searchLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.searchLayout.Controls.Add(this.flowLayoutPanel6, 0, 0);
            this.searchLayout.Controls.Add(this.flowLayoutPanel7, 1, 0);
            this.searchLayout.Controls.Add(this.searchFilterButton, 1, 1);
            this.searchLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchLayout.Location = new System.Drawing.Point(0, 0);
            this.searchLayout.Name = "searchLayout";
            this.searchLayout.RowCount = 2;
            this.searchLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.searchLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.searchLayout.Size = new System.Drawing.Size(573, 379);
            this.searchLayout.TabIndex = 0;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel6.Controls.Add(this.searchKeywordLabel);
            this.flowLayoutPanel6.Controls.Add(this.searchTypeLabel);
            this.flowLayoutPanel6.Controls.Add(this.searchCharacterLabel);
            this.flowLayoutPanel6.Controls.Add(this.searchSeriesLabel);
            this.flowLayoutPanel6.Controls.Add(this.searchPriceLabel);
            this.flowLayoutPanel6.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(60, 42);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(137, 239);
            this.flowLayoutPanel6.TabIndex = 0;
            // 
            // searchKeywordLabel
            // 
            this.searchKeywordLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.searchKeywordLabel.AutoSize = true;
            this.searchKeywordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchKeywordLabel.Location = new System.Drawing.Point(53, 0);
            this.searchKeywordLabel.Name = "searchKeywordLabel";
            this.searchKeywordLabel.Padding = new System.Windows.Forms.Padding(0, 14, 0, 13);
            this.searchKeywordLabel.Size = new System.Drawing.Size(76, 47);
            this.searchKeywordLabel.TabIndex = 1;
            this.searchKeywordLabel.Text = "Keyword";
            this.searchKeywordLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // searchTypeLabel
            // 
            this.searchTypeLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.searchTypeLabel.AutoSize = true;
            this.searchTypeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchTypeLabel.Location = new System.Drawing.Point(3, 47);
            this.searchTypeLabel.Name = "searchTypeLabel";
            this.searchTypeLabel.Padding = new System.Windows.Forms.Padding(0, 14, 0, 13);
            this.searchTypeLabel.Size = new System.Drawing.Size(126, 47);
            this.searchTypeLabel.TabIndex = 2;
            this.searchTypeLabel.Text = "Type of Goods";
            this.searchTypeLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // searchCharacterLabel
            // 
            this.searchCharacterLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.searchCharacterLabel.AutoSize = true;
            this.searchCharacterLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchCharacterLabel.Location = new System.Drawing.Point(41, 94);
            this.searchCharacterLabel.Name = "searchCharacterLabel";
            this.searchCharacterLabel.Padding = new System.Windows.Forms.Padding(0, 14, 0, 13);
            this.searchCharacterLabel.Size = new System.Drawing.Size(88, 47);
            this.searchCharacterLabel.TabIndex = 3;
            this.searchCharacterLabel.Text = "Character";
            this.searchCharacterLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // searchSeriesLabel
            // 
            this.searchSeriesLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.searchSeriesLabel.AutoSize = true;
            this.searchSeriesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchSeriesLabel.Location = new System.Drawing.Point(69, 141);
            this.searchSeriesLabel.Name = "searchSeriesLabel";
            this.searchSeriesLabel.Padding = new System.Windows.Forms.Padding(0, 14, 0, 13);
            this.searchSeriesLabel.Size = new System.Drawing.Size(60, 47);
            this.searchSeriesLabel.TabIndex = 4;
            this.searchSeriesLabel.Text = "Series";
            this.searchSeriesLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // searchPriceLabel
            // 
            this.searchPriceLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.searchPriceLabel.AutoSize = true;
            this.searchPriceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchPriceLabel.Location = new System.Drawing.Point(80, 188);
            this.searchPriceLabel.Name = "searchPriceLabel";
            this.searchPriceLabel.Padding = new System.Windows.Forms.Padding(0, 14, 0, 13);
            this.searchPriceLabel.Size = new System.Drawing.Size(49, 47);
            this.searchPriceLabel.TabIndex = 5;
            this.searchPriceLabel.Text = "Price";
            this.searchPriceLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.flowLayoutPanel7.Controls.Add(this.searchKeywordInput);
            this.flowLayoutPanel7.Controls.Add(this.searchTypeInput);
            this.flowLayoutPanel7.Controls.Add(this.searchCharacterInput);
            this.flowLayoutPanel7.Controls.Add(this.searchSeriesInput);
            this.flowLayoutPanel7.Controls.Add(this.flowLayoutPanel8);
            this.flowLayoutPanel7.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(203, 38);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(281, 243);
            this.flowLayoutPanel7.TabIndex = 1;
            // 
            // searchKeywordInput
            // 
            this.searchKeywordInput.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.searchKeywordInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchKeywordInput.Location = new System.Drawing.Point(3, 10);
            this.searchKeywordInput.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.searchKeywordInput.Name = "searchKeywordInput";
            this.searchKeywordInput.Size = new System.Drawing.Size(278, 26);
            this.searchKeywordInput.TabIndex = 0;
            // 
            // searchTypeInput
            // 
            this.searchTypeInput.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.searchTypeInput.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.searchTypeInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchTypeInput.FormattingEnabled = true;
            this.searchTypeInput.Location = new System.Drawing.Point(3, 56);
            this.searchTypeInput.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.searchTypeInput.Name = "searchTypeInput";
            this.searchTypeInput.Size = new System.Drawing.Size(278, 28);
            this.searchTypeInput.TabIndex = 1;
            // 
            // searchCharacterInput
            // 
            this.searchCharacterInput.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.searchCharacterInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchCharacterInput.Location = new System.Drawing.Point(3, 104);
            this.searchCharacterInput.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.searchCharacterInput.Name = "searchCharacterInput";
            this.searchCharacterInput.Size = new System.Drawing.Size(278, 26);
            this.searchCharacterInput.TabIndex = 2;
            // 
            // searchSeriesInput
            // 
            this.searchSeriesInput.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.searchSeriesInput.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.searchSeriesInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchSeriesInput.FormattingEnabled = true;
            this.searchSeriesInput.Location = new System.Drawing.Point(3, 150);
            this.searchSeriesInput.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.searchSeriesInput.Name = "searchSeriesInput";
            this.searchSeriesInput.Size = new System.Drawing.Size(278, 28);
            this.searchSeriesInput.TabIndex = 3;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.flowLayoutPanel8.Controls.Add(this.searchComparisonInput);
            this.flowLayoutPanel8.Controls.Add(this.searchPriceInput);
            this.flowLayoutPanel8.Location = new System.Drawing.Point(3, 191);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(278, 43);
            this.flowLayoutPanel8.TabIndex = 4;
            // 
            // searchComparisonInput
            // 
            this.searchComparisonInput.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.searchComparisonInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchComparisonInput.FormattingEnabled = true;
            this.searchComparisonInput.Location = new System.Drawing.Point(0, 10);
            this.searchComparisonInput.Margin = new System.Windows.Forms.Padding(0, 10, 3, 10);
            this.searchComparisonInput.Name = "searchComparisonInput";
            this.searchComparisonInput.Size = new System.Drawing.Size(132, 28);
            this.searchComparisonInput.TabIndex = 0;
            // 
            // searchPriceInput
            // 
            this.searchPriceInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchPriceInput.Location = new System.Drawing.Point(138, 10);
            this.searchPriceInput.Margin = new System.Windows.Forms.Padding(3, 10, 0, 10);
            this.searchPriceInput.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.searchPriceInput.Name = "searchPriceInput";
            this.searchPriceInput.Size = new System.Drawing.Size(131, 26);
            this.searchPriceInput.TabIndex = 1;
            // 
            // searchFilterButton
            // 
            this.searchFilterButton.Location = new System.Drawing.Point(203, 287);
            this.searchFilterButton.Name = "searchFilterButton";
            this.searchFilterButton.Size = new System.Drawing.Size(141, 24);
            this.searchFilterButton.TabIndex = 2;
            this.searchFilterButton.Text = "Search";
            this.searchFilterButton.UseVisualStyleBackColor = true;
            this.searchFilterButton.Click += new System.EventHandler(this.SearchFilterButton_Click);
            // 
            // AddPage
            // 
            this.AddPage.BackColor = System.Drawing.SystemColors.Control;
            this.AddPage.Controls.Add(this.inputForm);
            this.AddPage.Location = new System.Drawing.Point(4, 22);
            this.AddPage.Name = "AddPage";
            this.AddPage.Size = new System.Drawing.Size(573, 379);
            this.AddPage.TabIndex = 3;
            this.AddPage.Text = "AddNew";
            // 
            // inputForm
            // 
            this.inputForm.ColumnCount = 2;
            this.inputForm.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.inputForm.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.inputForm.Controls.Add(this.inputLabels, 0, 0);
            this.inputForm.Controls.Add(this.itemInputs, 1, 0);
            this.inputForm.Controls.Add(this.flowLayoutPanel4, 1, 1);
            this.inputForm.Controls.Add(this.IdInput, 0, 1);
            this.inputForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputForm.Location = new System.Drawing.Point(0, 0);
            this.inputForm.Name = "inputForm";
            this.inputForm.RowCount = 2;
            this.inputForm.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.inputForm.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.inputForm.Size = new System.Drawing.Size(573, 379);
            this.inputForm.TabIndex = 0;
            // 
            // inputLabels
            // 
            this.inputLabels.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.inputLabels.Controls.Add(this.descLabel);
            this.inputLabels.Controls.Add(this.typeLabel);
            this.inputLabels.Controls.Add(this.characterLabel);
            this.inputLabels.Controls.Add(this.seriesLabel);
            this.inputLabels.Controls.Add(this.priceLabel);
            this.inputLabels.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.inputLabels.Location = new System.Drawing.Point(60, 32);
            this.inputLabels.Name = "inputLabels";
            this.inputLabels.Size = new System.Drawing.Size(137, 230);
            this.inputLabels.TabIndex = 0;
            // 
            // descLabel
            // 
            this.descLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.descLabel.AutoSize = true;
            this.descLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.descLabel.Location = new System.Drawing.Point(29, 13);
            this.descLabel.Margin = new System.Windows.Forms.Padding(3, 13, 3, 13);
            this.descLabel.Name = "descLabel";
            this.descLabel.Size = new System.Drawing.Size(100, 20);
            this.descLabel.TabIndex = 0;
            this.descLabel.Text = "Description";
            this.descLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // typeLabel
            // 
            this.typeLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.typeLabel.AutoSize = true;
            this.typeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.typeLabel.Location = new System.Drawing.Point(3, 59);
            this.typeLabel.Margin = new System.Windows.Forms.Padding(3, 13, 3, 13);
            this.typeLabel.Name = "typeLabel";
            this.typeLabel.Size = new System.Drawing.Size(126, 20);
            this.typeLabel.TabIndex = 1;
            this.typeLabel.Text = "Type of Goods";
            this.typeLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // characterLabel
            // 
            this.characterLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.characterLabel.AutoSize = true;
            this.characterLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.characterLabel.Location = new System.Drawing.Point(41, 105);
            this.characterLabel.Margin = new System.Windows.Forms.Padding(3, 13, 3, 13);
            this.characterLabel.Name = "characterLabel";
            this.characterLabel.Size = new System.Drawing.Size(88, 20);
            this.characterLabel.TabIndex = 2;
            this.characterLabel.Text = "Character";
            this.characterLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // seriesLabel
            // 
            this.seriesLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.seriesLabel.AutoSize = true;
            this.seriesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seriesLabel.Location = new System.Drawing.Point(69, 151);
            this.seriesLabel.Margin = new System.Windows.Forms.Padding(3, 13, 3, 13);
            this.seriesLabel.Name = "seriesLabel";
            this.seriesLabel.Size = new System.Drawing.Size(60, 20);
            this.seriesLabel.TabIndex = 0;
            this.seriesLabel.Text = "Series";
            this.seriesLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // priceLabel
            // 
            this.priceLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.priceLabel.AutoSize = true;
            this.priceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.priceLabel.Location = new System.Drawing.Point(80, 197);
            this.priceLabel.Margin = new System.Windows.Forms.Padding(3, 13, 3, 13);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(49, 20);
            this.priceLabel.TabIndex = 3;
            this.priceLabel.Text = "Price";
            this.priceLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // itemInputs
            // 
            this.itemInputs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.itemInputs.Controls.Add(this.descInput);
            this.itemInputs.Controls.Add(this.typeInput);
            this.itemInputs.Controls.Add(this.charaInput);
            this.itemInputs.Controls.Add(this.seriesInput);
            this.itemInputs.Controls.Add(this.priceInput);
            this.itemInputs.Location = new System.Drawing.Point(203, 32);
            this.itemInputs.Name = "itemInputs";
            this.itemInputs.Size = new System.Drawing.Size(287, 230);
            this.itemInputs.TabIndex = 1;
            // 
            // descInput
            // 
            this.descInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.descInput.Location = new System.Drawing.Point(3, 10);
            this.descInput.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.descInput.Name = "descInput";
            this.descInput.Size = new System.Drawing.Size(278, 26);
            this.descInput.TabIndex = 0;
            // 
            // typeInput
            // 
            this.typeInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.typeInput.Location = new System.Drawing.Point(3, 56);
            this.typeInput.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.typeInput.Name = "typeInput";
            this.typeInput.Size = new System.Drawing.Size(278, 26);
            this.typeInput.TabIndex = 1;
            // 
            // charaInput
            // 
            this.charaInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.charaInput.Location = new System.Drawing.Point(3, 102);
            this.charaInput.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.charaInput.Name = "charaInput";
            this.charaInput.Size = new System.Drawing.Size(278, 26);
            this.charaInput.TabIndex = 2;
            // 
            // seriesInput
            // 
            this.seriesInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seriesInput.Location = new System.Drawing.Point(3, 148);
            this.seriesInput.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.seriesInput.Name = "seriesInput";
            this.seriesInput.Size = new System.Drawing.Size(278, 26);
            this.seriesInput.TabIndex = 3;
            // 
            // priceInput
            // 
            this.priceInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.priceInput.Location = new System.Drawing.Point(3, 194);
            this.priceInput.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.priceInput.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.priceInput.Name = "priceInput";
            this.priceInput.Size = new System.Drawing.Size(120, 26);
            this.priceInput.TabIndex = 5;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.saveButton);
            this.flowLayoutPanel4.Controls.Add(this.cancelButton);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(203, 268);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(287, 40);
            this.flowLayoutPanel4.TabIndex = 2;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(3, 10);
            this.saveButton.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(137, 24);
            this.saveButton.TabIndex = 0;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(146, 10);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(137, 24);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // IdInput
            // 
            this.IdInput.Location = new System.Drawing.Point(3, 268);
            this.IdInput.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.IdInput.Name = "IdInput";
            this.IdInput.Size = new System.Drawing.Size(120, 20);
            this.IdInput.TabIndex = 3;
            this.IdInput.Visible = false;
            // 
            // HomeScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 411);
            this.Controls.Add(this.homeInstructions);
            this.Name = "HomeScreen";
            this.Text = "Character Goods Inventory";
            this.homeInstructions.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ViewSwitch.ResumeLayout(false);
            this.HomePage.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.ViewPage.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.allData)).EndInit();
            this.flowLayoutPanel5.ResumeLayout(false);
            this.SearchPage.ResumeLayout(false);
            this.searchLayout.ResumeLayout(false);
            this.flowLayoutPanel6.ResumeLayout(false);
            this.flowLayoutPanel6.PerformLayout();
            this.flowLayoutPanel7.ResumeLayout(false);
            this.flowLayoutPanel7.PerformLayout();
            this.flowLayoutPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchPriceInput)).EndInit();
            this.AddPage.ResumeLayout(false);
            this.inputForm.ResumeLayout(false);
            this.inputLabels.ResumeLayout(false);
            this.inputLabels.PerformLayout();
            this.itemInputs.ResumeLayout(false);
            this.itemInputs.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.priceInput)).EndInit();
            this.flowLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.IdInput)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel homeInstructions;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button viewButton;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button CloseButton;
        private TablessTabControl ViewSwitch;
        private System.Windows.Forms.TabPage HomePage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label viewText;
        private System.Windows.Forms.Label searchText;
        private System.Windows.Forms.Label addText;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label viewLabel;
        private System.Windows.Forms.Label searchLabel;
        private System.Windows.Forms.Label addLabel;
        private System.Windows.Forms.TabPage ViewPage;
        private System.Windows.Forms.TabPage SearchPage;
        private System.Windows.Forms.TabPage AddPage;
        private System.Windows.Forms.TableLayoutPanel inputForm;
        private System.Windows.Forms.FlowLayoutPanel inputLabels;
        private System.Windows.Forms.Label descLabel;
        private System.Windows.Forms.Label typeLabel;
        private System.Windows.Forms.Label characterLabel;
        private System.Windows.Forms.Label seriesLabel;
        private System.Windows.Forms.Label priceLabel;
        private System.Windows.Forms.FlowLayoutPanel itemInputs;
        private System.Windows.Forms.TextBox descInput;
        private System.Windows.Forms.TextBox typeInput;
        private System.Windows.Forms.TextBox charaInput;
        private System.Windows.Forms.TextBox seriesInput;
        private System.Windows.Forms.NumericUpDown priceInput;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.DataGridView allData;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.NumericUpDown IdInput;
        private System.Windows.Forms.TableLayoutPanel searchLayout;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.Label searchKeywordLabel;
        private System.Windows.Forms.Label searchTypeLabel;
        private System.Windows.Forms.Label searchCharacterLabel;
        private System.Windows.Forms.Label searchSeriesLabel;
        private System.Windows.Forms.Label searchPriceLabel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.TextBox searchKeywordInput;
        private System.Windows.Forms.ComboBox searchTypeInput;
        private System.Windows.Forms.TextBox searchCharacterInput;
        private System.Windows.Forms.ComboBox searchSeriesInput;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.ComboBox searchComparisonInput;
        private System.Windows.Forms.NumericUpDown searchPriceInput;
        private System.Windows.Forms.Button searchFilterButton;
    }
}